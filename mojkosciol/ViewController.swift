//
//  ViewController.swift
//  mojkosciol
//
//  Created by Dawid Macura on 8/9/17.
//  Copyright © 2017 Dawid Macura. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        changeToMenuViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func changeToMenuViewController(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
           self.navigationController?.present(MenuViewController(), animated: true, completion: nil)
        })
    }
}

